import React, { useState } from "react";
import "./App.css";
import Sidebar from "./components/sideNavBar/sidebar";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import BooksPage from "./pages/BooksList";
import CategoriesPage from "./pages/CategoryList";
import SectionsPage from "./pages/SectionList";
import UserList from "./pages/UserList";
import CreateUser from "./pages/createuser/CreateUser";

function App() {
  const [showSidebar, setShowSidebar] = useState(false);

  return (
    <Router>
      <div className="App">
        <div className={`container ${showSidebar ? "show-sidebar" : ""}`}>
          <Sidebar />
        </div> 
        <div className="routes">
          <Routes>
            <Route path="/books" element={<BooksPage />} />
            <Route path="/categories" element={<CategoriesPage />} />
            <Route path="/sections" element={<SectionsPage />} />
            <Route path="/users" element={<UserList />} />
            <Route path="/" element={<BooksPage />} />
            <Route path="/create-user" element={<CreateUser />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
