import React, { useEffect, useState } from "react";
import DataTable from "../components/dataTable/datatable";
import EditIcon from "../assests/images/Edit Icon.svg";
import DeleteIcon from "../assests/images/Delete Icon.svg";
import Header from "../components/header/header";
import AddBookPopUp from "../components/popup/addBookPopup";
import EditBookPopup from "../components/popup/editBookPopup";
import instance from "../utils/instance";
import reportWebVitals from "../reportWebVitals";

interface BookData {
  id: number;
  name: string;
  categories: any[];
  sections: any[];
  createdDate: string;
  updatedDate: string;
  [key: string]: any;
}

const BooksPage: React.FC = () => {
  const [booksData, setBooksData] = useState<BookData[]>([]);
  const [isAddBookPopupOpen, setIsAddBookPopupOpen] = useState(false);
  const [isEditBookPopupOpen, setIsEditBookPopupOpen] = useState(false);
  const [selectedBook, setSelectedBook] = useState<BookData | null>(null);
  const [searchQuery, setSearchQuery] = useState("");
  const [sortKey, setSortKey] = useState<string | null>(null);
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const [booksResponse] = await Promise.all([instance.get("/book/get")]);
      setBooksData(booksResponse.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const handleAddBook = async () => {
    setIsAddBookPopupOpen(true);
  };

  const onAddBook = async () => {
    setIsAddBookPopupOpen(false);
    fetchData();
  };

  const handleEdit = (id: number) => {
    const selected = booksData.find((book) => book.id === id);
    if (selected) {
      setSelectedBook(selected);
      setIsEditBookPopupOpen(true);
    }
  };

  const handleDelete = (id: number) => {
    instance
      .delete(`/book/delete/${id}`)
      .then((response) => {
        if (response.status === 200) {
          console.log(`Book with ID ${id} was deleted successfully.`);
          setBooksData((prevData) => prevData.filter((book) => book.id !== id));
        } else {
          console.error(`Error deleting book with ID ${id}.`);
        }
      })
      .catch((error) => {
        console.error("Error deleting book:", error);
      });
  };

  const columns = [
    { field: "name", headerName: "Book Name", width: 200 },
    {
      field: "categories",
      headerName: "Category Name",
      width: 300,
      valueGetter: (params: any) => {
        return params.row.categories
          .map((category: any) => category.name)
          .join(", ");
      },
    },
    {
      field: "sections",
      headerName: "Section Name",
      width: 300,
      valueGetter: (params: any) => {
        return params.row.sections
          .map((section: any) => section.name)
          .join(", ");
      },
    },
    { field: "createdDate", headerName: "Created Date", width: 200 },
    { field: "updatedDate", headerName: "Updated Date", width: 200 },
    {
      field: "actions",
      headerName: "",
      width: 100,
      sortable: false,
      renderCell: (params: any) => (
        <div>
          <button
            className="edit-button"
            onClick={() => {
              handleEdit(params.row.id);
            }}
          >
            <img src={EditIcon} alt="Edit" />
          </button>
          <button
            className="delete-button"
            onClick={() => handleDelete(params.row.id)}
          >
            <img src={DeleteIcon} alt="Delete" />
          </button>
        </div>
      ),
    },
  ];

  const handleSearch = (query: string) => {
    setSearchQuery(query);
  };
  
  const filteredBooks = Array.isArray(booksData)
    ? booksData.filter((book) =>
        book.name.toLowerCase().includes(searchQuery.toLowerCase())
      )
    : [];

  const handleSort = (key: string) => {
    if (sortKey === key) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortKey(key);
      setSortOrder("asc");
    }
  };

  return (
    <>
      <Header title="Books" data={booksData} onSearch={handleSearch} />
      <DataTable
        data={filteredBooks}
        columns={columns}
        title="Book"
        onAddClick={handleAddBook}
        onClickSort={handleSort}
        sortKey={sortKey}
        sortOrder={sortOrder}
      />
      {isEditBookPopupOpen && selectedBook && (
        <EditBookPopup
          isOpen={isEditBookPopupOpen}
          onClose={() => {
            setIsEditBookPopupOpen(false);
            setSelectedBook(null);
          }}
          onUpdateClick={() => {
            setIsEditBookPopupOpen(false);
            setSelectedBook(null);
            fetchData();
          }}
          bookData={selectedBook}
        />
      )}
      {isAddBookPopupOpen && (
        <AddBookPopUp
          name="Book"
          onClose={() => setIsAddBookPopupOpen(false)}
          isOpen={isAddBookPopupOpen}
          onAddClick={onAddBook}
        />
      )}
    </>
  );
};

export default BooksPage;
