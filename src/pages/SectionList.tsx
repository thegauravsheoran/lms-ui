import React, { useEffect, useState } from "react";
import DataTable from "../components/dataTable/datatable";
import EditIcon from "../assests/images/Edit Icon.svg";
import DeleteIcon from "../assests/images/Delete Icon.svg";
import Header from "../components/header/header";
import AddSectionPopUp from "../components/popup/addSection";
import EditSectionPopup from "../components/popup/editSection";
import instance from "../utils/instance";

interface SectionData {
  id: number;
  name: string;
}

const SectionsPage: React.FC = () => {
  const [sectionsData, setSectionsData] = useState<SectionData[]>([]);
  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const [isAddSectionPopupOpen, setIsAddSectionPopupOpen] = useState(false);
  const [isEditSectionPopupOpen, setIsEditSectionPopupOpen] = useState(false);
  const [selectedSection, setSelectedSection] = useState<SectionData | null>(
    null
  );

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await instance.get("/sections/get");
      if (response.status === 200) {
        setSectionsData(response.data);
      } else {
        console.error("Error fetching sections data:", response.statusText);
      }
    } catch (error) {
      console.error("Error fetching sections data:", error);
    }
  };

  const handleEdit = (id: number) => {
    const selected = sectionsData.find((section) => section.id === id);
    if (selected) {
      setSelectedSection(selected);
      setIsEditSectionPopupOpen(true);
    }
  };

  const handleDelete = (id: number) => {
    instance
      .delete(`/sections/delete/${id}`)
      .then((response) => {
        if (response.status == 200) {
          console.log(`Section with ID ${id} was deleted successfully.`);
          setSectionsData((prevData) =>
            prevData.filter((item) => item.id !== id)
          );
        } else {
          console.error(`Error deleting section with ID ${id}.`);
        }
      })
      .catch((error) => {
        console.error("Error deleting section:", error);
      });
  };

  const columns = [
    { field: "name", headerName: "Section Name", width: 300 },
    {
      field: "actions",
      headerName: "",
      width: 160,
      sortable: false,
      renderCell: (params: any) => (
        <div>
          <button
            className="edit-button"
            onClick={() => {
              handleEdit(params.row.id);
              setIsPopupOpen(true);
            }}
          >
            <img src={EditIcon} alt="Edit" />
          </button>
          <button
            className="delete-button"
            onClick={() => handleDelete(params.row.id)}
          >
            <img src={DeleteIcon} alt="Delete" />
          </button>
        </div>
      ),
    },
  ];

  function handleAddSection(): void {
    console.log("Working");
    setIsAddSectionPopupOpen(true);
  }

  const handleSearch = (query: string) => {
    setSearchQuery(query);
  };

  const filteredSection = sectionsData.filter((section) =>
    section.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  function onAddSection(): void {
    setIsAddSectionPopupOpen(false);
    fetchData();
  }

  return (
    <>
      <Header title="Sections" data={sectionsData} onSearch={handleSearch} />
      <DataTable
        data={filteredSection}
        columns={columns}
        title="Section"
        onAddClick={handleAddSection}
      />
      {isEditSectionPopupOpen && selectedSection && (
        <EditSectionPopup
          isOpen={isEditSectionPopupOpen}
          onClose={() => setIsEditSectionPopupOpen(false)}
          onUpdateClick={handleEdit}
          sectionData={selectedSection}
        />
      )}

      {isAddSectionPopupOpen && (
        <AddSectionPopUp
          isOpen={isAddSectionPopupOpen}
          onClose={() => setIsAddSectionPopupOpen(false)}
          onAddClick={onAddSection}
          name="Section"
        />
      )}
    </>
  );
};

export default SectionsPage;
