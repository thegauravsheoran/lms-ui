import React, { useEffect, useState } from "react";
import DataTable from "../components/dataTable/datatable";
import EditIcon from "../assests/images/Edit Icon.svg";
import DeleteIcon from "../assests/images/Delete Icon.svg";
import PopupComponent from "../components/popup/popup";
import Header from "../components/header/header";
import CategoryPopup from "../components/popup/editCategoryPopUp";
import AddCategoryPopUp from "../components/popup/addCategory";
import axios from "axios";
import instance from "../utils/instance";

interface CategoryData {
  id: number;
  name: string;
}

const CategoriesPage: React.FC = () => {
  const [categoriesData, setCategoriesData] = useState<CategoryData[]>([]);
  const [isAddCategoryPopupOpen, setIsAddCategoryPopupOpen] = useState(false);
  const [isEditCategoryPopupOpen, setIsEditCategoryPopupOpen] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState<CategoryData | null>(
    null
  );
  const [searchQuery, setSearchQuery] = useState("");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await instance.get("/categories");
      if (response.status === 200) {
        setCategoriesData(response.data);
      } else {
        console.error("Error fetching categories data:", response.statusText);
      }
    } catch (error) {
      console.error("Error fetching categories data:", error);
    }
  };
  const handleEdit = (id: number) => {
    const selected = categoriesData.find((category) => category.id === id);
    if (selected) {
      setSelectedCategory(selected);
      setIsEditCategoryPopupOpen(true);
    }
  };

  const handleDelete = (id: number) => {
    instance
      .delete(`/categories/delete/${id}`)
      .then((response) => {
        if (response.status === 200) {
          console.log(`Book with ID ${id} was deleted successfully.`);
          setCategoriesData((prevData) =>
            prevData.filter((item) => item.id !== id)
          );
        } else {
          console.error(`Error deleting book with ID ${id}.`);
        }
      })
      .catch((error) => {
        console.error("Error deleting book:", error);
      });
  };

  function onAddCategory(): void {
    setIsAddCategoryPopupOpen(false);
    fetchData();
  }

  function handleAddCategory() {
    console.log("Working");
    setIsAddCategoryPopupOpen(true);
  }

  const handleSearch = (query: string) => {
    setSearchQuery(query);
  };

  const filteredCategories = categoriesData.filter((categories) =>
    categories.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const columns = [
    { field: "name", headerName: "Category Name", width: 300 },
    {
      field: "actions",
      headerName: "",
      width: 160,
      sortable: false,
      renderCell: (params: any) => (
        <div>
          <button
            className="edit-button"
            onClick={() => {
              handleEdit(params.row.id);
              setIsEditCategoryPopupOpen(true);
            }}
          >
            <img src={EditIcon} alt="Edit" />
          </button>
          <button
            className="delete-button"
            onClick={() => handleDelete(params.row.id)}
          >
            <img src={DeleteIcon} alt="Delete" />
          </button>
        </div>
      ),
    },
  ];

  return (
    <>
      <Header
        title="Categories"
        data={categoriesData}
        onSearch={handleSearch}
      />
      <DataTable
        data={filteredCategories}
        columns={columns}
        title="Category"
        onAddClick={handleAddCategory}
      />
      {isEditCategoryPopupOpen && selectedCategory && (
        <CategoryPopup
          isOpen={isEditCategoryPopupOpen}
          onClose={() => setIsEditCategoryPopupOpen(false)}
          onUpdateClick={handleEdit}
          categoryData={selectedCategory}
        />
      )}

      {isAddCategoryPopupOpen && (
        <AddCategoryPopUp
          isOpen={isAddCategoryPopupOpen}
          onClose={() => setIsAddCategoryPopupOpen(false)}
          onAddClick={onAddCategory}
          name="Category"
        />
      )}
    </>
  );
};

export default CategoriesPage;
