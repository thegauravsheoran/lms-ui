import React, { useEffect, useState } from "react";
import EditIcon from "../assests/images/Edit Icon.svg";
import DeleteIcon from "../assests/images/Delete Icon.svg";
import DataTable from "../components/dataTable/datatable";
import Header from "../components/header/header";
import adminIcon from "../assests/images/admin-icon.svg";
import userIcon from "../assests/images/userIcon2.svg";
import { useNavigate, useLocation } from "react-router";
import EditUserPopup from "../components/popup/editUser";

interface UserData {
  id: number;
  name: string;
  email: string;
  status: string;
  assignedBooks: string;
}

function UserList() {
  const [userData, setUserData] = useState<UserData[]>(
    require("../assests/jsonFiles/userData.json")
  );
  const [searchQuery, setSearchQuery] = useState("");
  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [editingUser, setEditingUser] = useState<UserData | null>(null);

  const navigate = useNavigate();
  const location = useLocation();

  const newUser = location.state && location.state.newUser;

  useEffect(() => {
    if (newUser) {
      console.log(newUser);
      const userExists = userData.some((user) => user.id === newUser.id);

      if (!userExists) {
        setUserData((prevUserData) => [...prevUserData, newUser]);
      }
    }
  }, [newUser, userData]);

  function handleEdit(id: number) {
    const userToEdit = userData.find((user) => user.id === id);
    if (userToEdit) {
      setEditingUser(userToEdit);
      setIsPopupOpen(true);
    }
  }

  function handleUpdateUser(updatedUser: UserData) {
    const updatedUserData = userData.map((user) =>
      user.id === updatedUser.id ? updatedUser : user
    );

    setUserData(updatedUserData);
    setIsPopupOpen(false);
  }

  function handleDelete(id: any): void {
    setUserData((prevUserData) =>
      prevUserData.filter((user) => user.id !== id)
    );
  }

  const handleSearch = (query: string) => {
    setSearchQuery(query);
  };

  const filteredUser = userData.filter((user) =>
    user.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  function handleAddUser(): void {
    navigate("/create-user");
  }

  const columns = [
    {
      field: "user",
      headerName: "User",
      width: 200,
      renderCell: (params: any) => (
        <div style={{ display: "flex", alignItems: "center" }}>
          <img
            src={adminIcon}
            alt="User Logo"
            style={{ marginRight: "8px", width: "24px", height: "24px" }}
          />
          {params.row.name}
        </div>
      ),
    },
    { field: "email", headerName: "Email", width: 300 },
    {
      field: "status",
      headerName: "Status",
      width: 250,
      valueGetter: (params: any) => {
        const status = params.row.status;
        const statusClass =
          status === "Active" ? "active-status" : "disable-status";
        return status;
      },
      cellClassName: (params: any) => {
        const status = params.value;
        return status === "Active" ? "active-status" : "disable-status";
      },
    },
    { field: "assignedBooks", headerName: "Assigned Books", width: 250 },
    {
      field: "actions",
      headerName: "",
      width: 160,
      sortable: false,
      renderCell: (params: any) => (
        <div>
          <button
            className="edit-button"
            onClick={() => {
              handleEdit(params.row.id);
            }}
          >
            <img src={EditIcon} alt="Edit" />
          </button>
          <button
            className="delete-button"
            onClick={() => handleDelete(params.row.id)}
          >
            <img src={DeleteIcon} alt="Delete" />
          </button>
        </div>
      ),
    },
  ];

  return (
    <>
      <Header title="User" data={userData} onSearch={handleSearch} />
      <DataTable
        data={filteredUser}
        columns={columns}
        title="User"
        onAddClick={handleAddUser}
      />
      {isPopupOpen && (
        <EditUserPopup
          userData={editingUser!}
          isOpen={isPopupOpen}
          onClose={() => setIsPopupOpen(false)}
          onUpdateClick={handleUpdateUser}
        />
      )}
    </>
  );
}

export default UserList;
