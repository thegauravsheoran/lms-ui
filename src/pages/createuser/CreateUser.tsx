import React, { useEffect, useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import UserAdd from "../../assests/images/User Add.svg";
import UserSubmit from '../../assests/images/User Check.svg'
import { useNavigate } from "react-router-dom";
import "./createuser.scss";
import instance from "../../utils/instance";

const initialValues = {
  name: "",
  email: "",
  status: "",
  assignedBooks: "",
};

const validationSchema = Yup.object({
  name: Yup.string().required("*Required full name"),
  email: Yup.string().required("*Required email"),
  status: Yup.string().required("*Required Status"),
  assignedBooks: Yup.string().required("*Select Books"),
});

function UserForm() {
  const navigate = useNavigate();
  const [bookOptions, setBookOptions] = useState([]);
  const [isSubmitting, setIsSubmitting] = useState(false); 


  useEffect(() => {
    instance.get("/book/get").then((response) => {
      const books = response.data;
      const bookNames = books.map((book: any) => book.name);
      setBookOptions(bookNames);
    });
  }, []);

  const handleSubmit = async (values: any) => {
    try {
      setIsSubmitting(true);
      setTimeout(() => {
        const newUser = {
          id: Date.now(),
          ...values,
        };
        navigate("/users", { state: { newUser } });
      }, 1500);
    } catch (error) {
      console.error("Error in form submission:", error);
    }
  };
  
  const numberOfRows = 3;

  return (
    <div className="user-form">
      <h2>Create User</h2>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        <Form>
        {/* {Array.from({ length: numberOfRows }, (_, index) => ( */}
          <div className="form-table">
            <div className="form-row">
              <div className="form-col">
                <label htmlFor="name">Name</label>
                <Field type="text" id="name" name="name" />
                <ErrorMessage
                  name="name"
                  component="div"
                  className="error-message"
                />
              </div>
              <div className="form-col">
                <label htmlFor="email">Email</label>
                <Field type="text" id="email" name="email" />
                <ErrorMessage
                  name="email"
                  component="div"
                  className="error-message"
                />
                
              </div>
              <div className="form-col">
                <label htmlFor="status">Status</label>
                <Field as="select" id="status" name="status">
                  <option value="" disabled>
                    Select Status
                  </option>
                  <option value="Active">Active</option>
                  <option value="Inactive">Inactive</option>
                </Field>
                <ErrorMessage
                  name="status"
                  component="div"
                  className="error-message"
                />
              </div>
              <div className="form-col">
                <label htmlFor="assignedBooks">Select Books</label>
                <Field as="select" id="assignedBooks" name="assignedBooks">
                  <option value="" disabled>
                    Select Books
                  </option>
                  {bookOptions.map((bookName, index) => (
                    <option key={index} value={bookName}>
                      {bookName}
                    </option>
                  ))}
                </Field>
                <ErrorMessage
                  name="assignedBooks"
                  component="div"
                  className="error-message"
                />
              </div>
            </div>
            <button className="add-user-button" type="submit">
            {isSubmitting ? (
                <img src={UserSubmit} alt="Submitting" />
              ) : (
                <img src={UserAdd} alt="Submit" />
              )}
            </button>
          </div>
       
        </Form>
        
      </Formik>
    </div>
  );
}

export default UserForm;
