import React, { useState } from "react";
import searchIcon from "../../assests/images/search-icon.svg";
import clearIcon from "../../assests/images/clearIcon.svg";
import "./header.scss";

interface HeaderProps {
  title: string;
  data?: any[];
  setData?: (data: any[]) => void;
  onSearch?: (query: string) => void;
}

const Header: React.FC<HeaderProps> = ({ title, data, setData, onSearch }) => {
  const [searchQuery, setSearchQuery] = useState("");

  const handleSearchInputChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const query = event.target.value;
    setSearchQuery(query);

    if (onSearch) {
      onSearch(query);
    }

    if (data && setData) {
      const filteredData = data.filter(
        (item: any) =>
          item.name || item.name.toLowerCase().includes(query.toLowerCase())
      );
      setData(filteredData);
    }
  };

  const clearSearch = () => {
    setSearchQuery("");
  };

  return (
    <div className="header">
      <div className="top-bar">
        <h1>{title} List</h1>
        <div className="search-container">
          <img src={searchIcon} alt="Search" className="search-icon" />
          <input
            name="input-box"
            placeholder={`Search ${title}`}
            value={searchQuery}
            onChange={handleSearchInputChange}
          />
          {
            <img
              src={clearIcon}
              alt="Clear"
              className="clear-icon"
              onClick={clearSearch}
            />
          }
        </div>
      </div>
    </div>
  );
};

export default Header;
