import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";

interface EditUserPopupProps {
  isOpen: boolean;
  onClose: () => void;
  onUpdateClick: (userData: any) => void;
  userData: {
    id: number;
    name: string;
    email: string;
    status: string;
    assignedBooks: string;
  };
}

const EditUserPopup: React.FC<EditUserPopupProps> = ({
  isOpen,
  onClose,
  onUpdateClick,
  userData,
}) => {
  const [name, setName] = useState(userData.name);
  const [email, setEmail] = useState(userData.email);
  const [status, setStatus] = useState(userData.status);
  const [assignedBooks, setAssignedBooks] = useState(userData.assignedBooks);

  useEffect(() => {
    if (isOpen) {
      setName(userData.name);
      setEmail(userData.email);
      setStatus(userData.status);
      setAssignedBooks(userData.assignedBooks);
    }
  }, [isOpen, userData]);

  const handleUpdate = () => {
    const updatedUserData = {
      id: userData.id,
      name: name,
      email: email,
      status: status,
      assignedBooks: assignedBooks,
    };

    onUpdateClick(updatedUserData);

    onClose();
  };

  return (
    <div>
      <Dialog open={isOpen} onClose={onClose} className="dialog-container">
        <DialogTitle>Edit User</DialogTitle>
        <hr />
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            type="text"
            variant="outlined"
            fullWidth
            required
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
          <TextField
            margin="dense"
            id="email"
            label="Email"
            type="text"
            variant="outlined"
            fullWidth
            required
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
          <FormControl fullWidth variant="outlined">
            <InputLabel id="status-label">Status</InputLabel>
            <Select
              labelId="status-label"
              id="editstatus"
              value={status}
              onChange={(event) => setStatus(event.target.value as string)}
              label="Status*"
              required
            >
              <MenuItem value="Active">Active</MenuItem>
              <MenuItem value="Disable">Disable</MenuItem>
            </Select>
          </FormControl>
          <TextField
            margin="dense"
            id="editassignedBooks"
            label="Assigned Books"
            type="text"
            variant="outlined"
            fullWidth
            value={assignedBooks}
            onChange={(event) => setAssignedBooks(event.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button className="popup-cancel-button" onClick={onClose}>
            Cancel
          </Button>
          <Button className="popup-add-button" onClick={handleUpdate}>
            Update User
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditUserPopup;
