import React, { useState } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import instance from "../../utils/instance";
import PopupComponent from "./popup";

interface editSectionPopupProps {
  isOpen: boolean;
  onClose: () => void;
  onUpdateClick: (categoryData: any) => void;
  sectionData: {
    id: number;
    name: string;
  };
}

const EditSectionPopup: React.FC<editSectionPopupProps> = ({
  isOpen,
  onClose,
  onUpdateClick,
  sectionData,
}) => {
  const [title, setTitle] = useState(sectionData.name);

  const handleUpdate = async () => {
    try {
      const updatedSectionData = {
        id: sectionData.id,
        name: title,
      };

      const response = await instance.put(
        `/sections/update/${sectionData.id}`,
        updatedSectionData
      );

      if (response.status === 200) {
        console.log("Section updated successfully");
        onUpdateClick(updatedSectionData);
        setTitle(updatedSectionData.name);
        onClose();
      } else {
        console.error("Error updating section");
      }
    } catch (error) {
      console.error("Error updating section:", error);
    }
  };

  return (
    <div>
      <Dialog open={isOpen} onClose={onClose} className="dialog-container">
        <DialogTitle>Edit Category</DialogTitle>
        <hr />
        <DialogContent>
        <PopupComponent
            inputLabel="Section Name"
            onPopupValue={(value) => setTitle(value)}
            title={title}
          />
        </DialogContent>
        <DialogActions>
          <Button className="popup-cancel-button" onClick={onClose}>
            Cancel
          </Button>
          <Button className="popup-add-button" onClick={handleUpdate}>
            Update Section
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditSectionPopup;
