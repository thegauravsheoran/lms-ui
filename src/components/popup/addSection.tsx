import React, { useState } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import "./popup.scss";
import instance from "../../utils/instance";
import PopupComponent from "./popup";

interface AddSectionPopupComponentProps {
  name: string;
  onClose: () => void;
  isOpen: boolean;
  onAddClick: () => void;
}

export default function AddSectionPopUp({
  name,
  onClose,
  isOpen,
  onAddClick,
}: AddSectionPopupComponentProps) {
  const [title, setTitle] = useState("");

  const handleClose = () => {
    onClose();
  };

  const handleSubmit = async () => {
    try {
      const newSection = {
        name: title,
      };

      const response = await instance.post("/sections/add", newSection);

      if (response.status === 200) {
        console.log("Section added successfully");
        onAddClick();
        handleClose();
      } else {
        console.error("Error adding section");
      }
    } catch (error) {
      console.error("Error adding section:", error);
    }
  };

  const isAddButtonDisabled = title.length === 0;

  return (
    <div>
      <Dialog open={isOpen} onClose={handleClose} className="dialog-container">
        <DialogTitle>Add {name}</DialogTitle>
        <hr />
        <DialogContent>
          <PopupComponent
            inputLabel="Section Name"
            onPopupValue={(value) => setTitle(value)}
            title={title}
          />
        </DialogContent>
        <DialogActions>
          <Button className="popup-cancel-button" onClick={handleClose}>
            Cancel
          </Button>
          <Button className="popup-add-button" onClick={handleSubmit} disabled={isAddButtonDisabled}>
            Add {name}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
