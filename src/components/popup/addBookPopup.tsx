import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import "./popup.scss";
import instance from "../../utils/instance";
import PopupComponent from "./popup";

interface BookPopupComponentProps {
  name?: string;
  onClose: () => void;
  isOpen?: boolean;
  onAddClick: () => void;
}

export default function AddBookPopUp({
  name,
  onClose,
  isOpen = false,
  onAddClick,
}: BookPopupComponentProps) {
  const [open, setOpen] = useState(isOpen);
  const [selectedCategories, setSelectedCategories] = useState([]);
  const [selectedSections, setSelectedSections] = useState([]);
  const [title, setTitle] = useState("");
  const [categoryList, setCategoryList] = useState<any[]>([]);
  const [sectionList, setSectionList] = useState<any[]>([]);

  useEffect(() => {
    if (isOpen) {
      fetchCategoriesAndSections();
    }
  }, [isOpen]);

  const fetchCategoriesAndSections = async () => {
    try {
      const [categoriesResponse, sectionsResponse] = await Promise.all([
        instance.get("/categories"),
        instance.get("/sections/get"),
      ]);

      setCategoryList(categoriesResponse.data);
      setSectionList(sectionsResponse.data);
    } catch (error) {
      console.error("Error fetching categories and sections:", error);
    }
  };

  const handleClose = () => {
    setOpen(false);
    onClose();
  };

  const handleSubmit = async () => {
    try {
      const newBook = {
        name: title,
        categories: selectedCategories,
        sections: selectedSections,
      };

      if (selectedCategories.length > 0) {
        newBook.categories = selectedCategories;
      }

      if (selectedSections.length > 0) {
        newBook.sections = selectedSections;
      }

      const response = await instance.post("/book/create", newBook);

      if (response.status === 200) {
        console.log("Book added successfully");
        onAddClick();
        handleClose();
      } else {
        console.error("Error adding book");
      }
    } catch (error) {
      console.error("Error adding book:", error);
    }
    onClose();
  };

  const handleCategoryChange = (event: any) => {
    setSelectedCategories(event.target.value);
  };

  const handleSectionChange = (event: any) => {
    setSelectedSections(event.target.value);
  };

  const isAddButtonDisabled = title.length === 0;

  return (
    <div>
      <Dialog open={isOpen} onClose={handleClose} className="dialog-container">
        <DialogTitle>Add {name}</DialogTitle>
        <hr />
        <DialogContent>
          <PopupComponent
            inputLabel="Book Name"
            onPopupValue={(value) => setTitle(value)}
            title={title}
          />
          {categoryList && categoryList.length > 0 && (
            <FormControl fullWidth>
              <InputLabel id="categories-label">Categories</InputLabel>
              <Select
                labelId="categories-label"
                id="categories"
                multiple
                value={selectedCategories}
                onChange={handleCategoryChange}
                label="Categories*"
                variant="outlined"
                required
              >
                {categoryList &&
                  categoryList.map((category: any) => (
                    <MenuItem key={category.id} value={category.id}>
                      {category.name}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>
          )}
          {sectionList && sectionList.length > 0 && (
            <FormControl fullWidth>
              <InputLabel id="sections-label">Sections</InputLabel>
              <Select
                labelId="sections-label"
                id="sections"
                multiple
                value={selectedSections}
                variant="outlined"
                onChange={handleSectionChange}
                label="Sections*"
                required
              >
                {sectionList &&
                  sectionList.map((section: any) => (
                    <MenuItem key={section.id} value={section.id}>
                      {section.name}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>
          )}
        </DialogContent>
        <DialogActions>
          <Button className="popup-cancel-button" onClick={handleClose}>
            Cancel
          </Button>
          <Button className="popup-add-button" onClick={handleSubmit} disabled={isAddButtonDisabled}>
            Add {name}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
