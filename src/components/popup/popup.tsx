import TextField from "@mui/material/TextField";
import "./popup.scss";
import { useState, useEffect } from "react";

interface PopupComponentProps {
  onPopupValue: (value: string) => void;
  inputLabel: string;
  title: string; 
}

export default function PopupComponent({
  onPopupValue,
  inputLabel,
  title, 
}: PopupComponentProps) {
  const [localTitle, setLocalTitle] = useState(title);

  useEffect(() => {
    setLocalTitle(title);
  }, [title]);

  const handleValueChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = event.target.value;
    setLocalTitle(newValue); 
    onPopupValue(newValue); 
  };

  return (
    <div>
      <TextField
        autoFocus
        margin="dense"
        className="title"
        id="title"
        label={inputLabel}
        type="text"
        variant="outlined"
        value={localTitle}
        onChange={handleValueChange}
        fullWidth
        required
      />
    </div>
  );
}
