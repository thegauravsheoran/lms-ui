import React, { useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import "./popup.scss";
import instance from "../../utils/instance";
import PopupComponent from "./popup";

interface AddCategoryPopupComponentProps {
  name: string;
  onClose: () => void;
  isOpen: boolean;
  onAddClick: () => void;
}

export default function AddCategoryPopUp({
  name,
  onClose,
  isOpen,
  onAddClick,
}: AddCategoryPopupComponentProps) {
  const [title, setTitle] = useState("");

  const handleClose = () => {
    onClose();
  };

  const handleSubmit = async () => {
    try {
      const newCategory = {
        name: title,
      };

      const response = await instance.post("/categories/add", newCategory);

      if (response.status === 200) {
        console.log("Category added successfully");
        onAddClick();
        handleClose();
      } else {
        console.error("Error adding category");
      }
    } catch (error) {
      console.error("Error adding category:", error);
    }
  };

  const isAddButtonDisabled = title.length === 0;

  return (
    <div>
      <Dialog open={isOpen} onClose={handleClose} className="dialog-container">
        <DialogTitle>Add {name}</DialogTitle>
        <hr />
        <DialogContent>
          <PopupComponent
            inputLabel="Category Name"
            onPopupValue={(value) => setTitle(value)}
            title={title}
          />
        </DialogContent>
        <DialogActions>
          <Button className="popup-cancel-button" onClick={handleClose}>
            Cancel
          </Button>
          <Button
            className="popup-add-button"
            onClick={handleSubmit}
            disabled={isAddButtonDisabled}
          >
            Add {name}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
