import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import instance from "../../utils/instance";
import PopupComponent from "./popup";

interface EditBookPopupProps {
  isOpen: boolean;
  onClose: () => void;
  onUpdateClick: (bookData: any) => void;
  bookData: {
    id: number;
    name: string;
    categories: any[]; 
    sections: any[]; 
  };
}

const EditBookPopup: React.FC<EditBookPopupProps> = ({
  isOpen,
  onClose,
  onUpdateClick,
  bookData,
}) => { 
  const [title, setTitle] = useState(bookData.name);
  const [selectedCategories, setSelectedCategories] = useState(bookData.categories);
  const [selectedSections, setSelectedSections] = useState(bookData.sections);
  const [categoryList, setCategoryList] = useState<any[]>([]);
  const [sectionList, setSectionList] = useState<any[]>([]);

  useEffect(() => {
    if (isOpen) {
      setSelectedCategories([]);
      setSelectedSections([]);
      fetchCategoriesAndSections();
    }
  }, [isOpen]);

  const fetchCategoriesAndSections = async () => {
    try {
      const [categoriesResponse, sectionsResponse] = await Promise.all([
        instance.get("/categories"),
        instance.get("/sections/get"),
      ]);

      setCategoryList(categoriesResponse.data);
      setSectionList(sectionsResponse.data);
    } catch (error) {
      console.error("Error fetching categories and sections:", error);
    }
  };

  const handleUpdate = async () => {
    try {
      const updatedBookData = {
        id: bookData.id,
        name: title,
        categories: selectedCategories,
        sections: selectedSections,
      };

      const response = await instance.put(
        `/book/update/${bookData.id}`,
        updatedBookData
      );

      if (response.status === 200) {
        console.log("Book updated successfully");
        onUpdateClick(updatedBookData);
        onClose();
      } else {
        console.error("Error updating book");
      }
    } catch (error) {
      console.error("Error updating book:", error);
    }
  };

  return (
    <div>
      <Dialog open={isOpen} onClose={onClose} className="dialog-container">
        <DialogTitle>Edit Book</DialogTitle>
        <hr />
        <DialogContent>
        <PopupComponent
            inputLabel="Book Name"
            onPopupValue={(value) => setTitle(value)}
            title={title}
          />
          <FormControl fullWidth>
            <InputLabel id="categories-label">Categories</InputLabel>
            <Select
              labelId="categories-label"
              id="categories"
              multiple
              value={selectedCategories}
              onChange={(event:any) => setSelectedCategories(event.target.value)}
              label="Categories*"
              variant="outlined"
              required
            >
              {categoryList.map((category: any) => (
                <MenuItem key={category.id} value={category.id}>
                  {category.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl fullWidth>
            <InputLabel id="sections-label">Sections</InputLabel>
            <Select
              labelId="sections-label"
              id="sections"
              multiple
              value={selectedSections}
              onChange={(event:any) => setSelectedSections(event.target.value)}
              required
            >
              {sectionList.map((section: any) => (
                <MenuItem key={section.id} value={section.id}>
                  {section.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button className="popup-cancel-button" onClick={onClose}>
            Cancel
          </Button>
          <Button className="popup-add-button" onClick={handleUpdate}>
            Update Book
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditBookPopup;
