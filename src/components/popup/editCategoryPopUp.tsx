import React, { useState } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import instance from "../../utils/instance";
import PopupComponent from "./popup";

interface CategoryPopupProps {
  isOpen: boolean;
  onClose: () => void;
  onUpdateClick: (categoryData: any) => void;
  categoryData: {
    id: number;
    name: string;
  };
}

const CategoryPopup: React.FC<CategoryPopupProps> = ({
  isOpen,
  onClose,
  onUpdateClick,
  categoryData,
}) => {
  const [title, setTitle] = useState(categoryData.name);

  const handleUpdate = async () => {
    try {
      const updatedCategoryData = {
        id: categoryData.id,
        name: title,
      };

      const response = await instance.put(
        `/categories/update/${categoryData.id}`,
        updatedCategoryData
      );

      if (response.status === 200) {
        console.log("Category updated successfully");
        onUpdateClick(updatedCategoryData);
        onClose();
      } else {
        console.error("Error updating category");
      }
    } catch (error) {
      console.error("Error updating category:", error);
    }
  };

  return (
    <div>
      <Dialog open={isOpen} onClose={onClose} className="dialog-container">
        <DialogTitle>Edit Category</DialogTitle>
        <hr />
        <DialogContent>
          <PopupComponent
            inputLabel="Category Name"
            onPopupValue={(value) => setTitle(value)}
            title={title}
          />
        </DialogContent>
        <DialogActions>
          <Button className="popup-cancel-button" onClick={onClose}>
            Cancel
          </Button>
          <Button className="popup-add-button" onClick={handleUpdate}>
            Update Category
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default CategoryPopup;
