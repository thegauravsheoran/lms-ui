import React, { useEffect, useState } from "react";
import "./datatable.scss";
import sortIcon from "../../assests/images/sortIcon.svg";
import filterIcon from "../../assests/images/filterIcon.svg";
import {
  DataGrid,
  GridColDef,
  GridFooter,
  GridFooterContainer,
} from "@mui/x-data-grid";

interface DataTableProps {
  data: any[];
  columns: GridColDef[];
  title: string;
  onAddClick: () => void;
  onClickSort?: (key: string) => any;
  sortKey?: string | null;
  sortOrder?: "asc" | "desc";
}

const DataTable: React.FC<DataTableProps> = ({
  data,
  columns,
  title,
  onAddClick,
  onClickSort,
  sortKey,
  sortOrder,
}) => {
  function onClickFilter(event: any): void {
    throw new Error("Function not implemented.");
  }


  return (
    <div className="datagrid-container">
      <div className="button-conatiner">
        <button onClick={onClickFilter} className="filter-button">
          <img src={filterIcon} alt="Filter" />
        </button>
        <button
          onClick={() => onClickSort && onClickSort("createdDate")}
          className="sort-button"
        >
          <img src={sortIcon} alt="Sort" />
          Sort: Chronological
        </button>
        <button onClick={onAddClick} className="add-button">
          Add {title}
        </button>
      </div>
      {data.length === 0 ? (
        <p>No {title} data available.</p>
      ) : (
        <DataGrid
          rows={data}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 10 },
            },
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
          className="datagrid"
          sortingMode="server"
          sortModel={[
            {
              field: sortKey || "createdDate",
              sort: sortOrder || "asc",
            },
          ]}
        />
      )}
    </div>
  );
};

export default DataTable;
