import React from "react";
import { Link } from "react-router-dom";
import "./sidebar.scss";
import adminIcon from "../../assests/images/admin-icon.svg";
import signOut from "../../assests/images/signOut.svg";
import categoryIcon from "../../assests/images/category-icon.webp";
import bookList from "../../assests/images/book-list.png";
import userIcon from "../../assests/images/user.svg";
import sectionList from "../../assests/images/section-logo.png";
import menu from "../../assests/images/menu.svg";

const Sidebar: React.FC = () => {
  return (
    <div className="sidebar">
      <div className="sidebar-admin">
        <img src={adminIcon}></img>
        <h5>Welcome</h5>
        <h3>Gaurav S</h3>
      </div>
      <div className="sidebar-items">
        <ul>
          <li>
            <Link to="/users">
              <img className="icon" src={userIcon}></img>User List
              <img className="menu" src={menu}></img>
            </Link>
          </li>
          <li>
            <Link to="/books">
              <img className="icon" src={bookList}></img>Book
              <img className="menu" src={menu}></img>
            </Link>
          </li>
          <li>
            <a href="/categories">
              <img className="icon" src={categoryIcon}></img>Categories
              <img className="menu" src={menu}></img>
            </a>
          </li>
          <li>
            <a href="/sections">
              <img className="icon" src={sectionList}></img>Section
              <img className="menu" src={menu}></img>
            </a>
          </li>
        </ul>
        <div className="singOut">
        <a href="/signOut">
          <img className="icon" src={signOut}></img>
        </a>
      </div>
      </div>
      
    </div>
  );
};

export default Sidebar;
